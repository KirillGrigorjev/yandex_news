Rails.application.routes.draw do
  root 'home#index'
  resource :admin, only: [:show, :create]
  mount ActionCable.server => '/cable'
end
