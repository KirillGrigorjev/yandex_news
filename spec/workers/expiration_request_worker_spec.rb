require 'rails_helper'

describe ExpirationRequestWorker, type: :model do
  let(:news_params) do
    { title: 'Title', annotation: 'Annotation', uuid: SecureRandom.uuid, published_at: Time.current }
  end
  let(:set_current_news) { RedisClient.set('current_news', news_params.to_json) }
  let(:instance) { described_class.new }

  before(:each) do
    stub_const('RedisClient', MockRedis.new)
  end

  describe 'perform' do
    it 'the same uuid' do
      set_current_news
      RedisClient.set('admin_news', news_params.to_json)
      expect_any_instance_of(MainYandexNews).to receive(:set_current_news)
      instance.perform(news_params[:uuid])
      expect(RedisClient.get('admin_news')).to be nil
    end

    it 'admin news already changed' do
      set_current_news
      expect_any_instance_of(MainYandexNews).not_to receive(:set_current_news)
      instance.perform(news_params[:uuid])
    end
  end
end
