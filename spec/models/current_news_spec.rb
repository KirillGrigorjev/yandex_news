require 'rails_helper'

describe CurrentNews, type: :model do
  let(:news_params) do
    { title: 'Title', annotation: 'Annotation', uuid: SecureRandom.uuid, published_at: Time.current }
  end
  let(:blank_instance) { described_class.new }
  let(:set_current_news) { RedisClient.set('current_news', news_params.to_json) }

  before(:each) do
    stub_const('RedisClient', MockRedis.new)
  end

  describe 'class methods' do
    describe 'retrieve_object' do
      it 'existing object' do
        set_current_news
        expect(described_class.retrieve_object.attributes)
          .to match(a_hash_including(news_params.except(:published_at)))
      end

      it 'black object' do
        expect(described_class.retrieve_object).to match an_instance_of(described_class)
        expect(described_class.retrieve_object.attributes.values.compact).to eq []
      end
    end

    describe 'submit' do
      context 'force: true' do
        it do
          expect_any_instance_of(ActionCable::Server::Base)
            .to receive(:broadcast).with(
              'current_news_channel',
              an_instance_of(ActiveModelSerializers::SerializableResource)
            )
          expect(blank_instance.submit(news_params, true)).to be true
        end
      end

      context 'force: false' do
        it 'admin news exists' do
          expect_any_instance_of(ActionCable::Server::Base).not_to receive(:broadcast)
          RedisClient.set('admin_news', news_params.to_json)
          set_current_news
          expect(described_class.retrieve_object.submit(news_params, false)).to be true
        end
      end
    end
  end
end
