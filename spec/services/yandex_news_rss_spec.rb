require 'rails_helper'

describe YandexNewsRss, type: :model do
  let(:expected_url) { 'https://news.yandex.ru/index.rss' }
  let!(:instance) { described_class.new(:index) }

  describe 'initialize' do
    it do
      expect(instance.instance_variable_get(:@url)).to eq(expected_url)
    end
  end

  describe 'retrieve_fresh_news' do
    it do
      stub_request(:get, expected_url).to_return(
        body: File.read(File.join([Rails.root, 'spec/fixtures/rss/index.rss']))
      )
      expect(instance.retrieve_fresh_news)
        .to match(a_hash_including(title: 'Test title', annotation: 'Test annotation'))
    end
  end
end
