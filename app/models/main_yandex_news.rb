class MainYandexNews < News
  include ActiveAttributes

  after_submit :set_current_news

  def set_current_news
    CurrentNews.refresh_news(attributes)
  end
end
