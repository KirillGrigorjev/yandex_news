class CurrentNews < News
  include ActiveAttributes

  after_submit :notify_users

  def self.refresh_news(params, force: false)
    retrieve_object.submit(params, force)
  end

  def submit(params, force)
    params = params.slice(*ATTRS)
    return super(params) if force || admin_news_expired?
    true
  end

  private

  def admin_news_expired?
    uuid.blank? || AdminNews.retrieve_object.uuid != uuid
  end

  def notify_users
    ActionCable.server.broadcast(NewsChannel::CHANNEL_NAME, serialize)
  end
end
