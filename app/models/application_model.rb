class ApplicationModel
  include ActiveModel::Model
  include ActiveModel::AttributeMethods
  include ActiveModel::Serialization

  define_model_callbacks :submit

  class << self
    def redis_key
      model_name.singular
    end

    def retrieve_object
      new(
        begin
          JSON.parse(RedisClient.get(redis_key))
        rescue TypeError, JSON::ParserError
          {}
        end
      )
    end
  end

  def persisted?
    false
  end

  def initialize(attrs = {})
    assign_attributes(attrs)
  end

  def submit(params)
    run_callbacks __method__ do
      sanitize_params!(params)
      return true if changes_empty?(params)
      set_params(params)
      return false if invalid?
      save_object
      true
    end
  end

  def attributes
    raise NotImplementedError
  end

  def delete
    RedisClient.del(self.class.redis_key)
  end

  def serialize
    ActiveModelSerializers::SerializableResource.new self
  end

  private

  def sanitize_params!(params)
    raise NotImplementedError
  end

  def changes_empty?(params = {})
    return @changes_empty if defined? @changes_empty
    current_attrs = attributes
    params.each do |name, value|
      next if current_attrs[name.to_sym] == value
      return @changes_empty = false
    end
    @changes_empty = true
  end

  def set_params(params)
    assign_attributes(params)
  end

  def save_object
    set_uuid
    RedisClient.set(self.class.redis_key, to_json)
  end

  def set_uuid
    self.uuid ||= SecureRandom.hex
  end
end
