class News < ApplicationModel
  ATTRS = %i(uuid title annotation published_at).freeze
  DATE_ATTRS = %i(published_at).freeze

  private

  def sanitize_params!(params)
    params.transform_keys!(&:to_sym)
    params[:published_at] = sanitize_date(params[:published_at])
  end
end
