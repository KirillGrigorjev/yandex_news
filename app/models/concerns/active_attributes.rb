module ActiveAttributes
  extend ActiveSupport::Concern

  included do |base|
    include ActiveModel::Serializers::JSON

    attr_accessor *(base::ATTRS - base::DATE_ATTRS)

    base::DATE_ATTRS.each do |attr|
      instance_variable_name = "@#{attr}"

      define_method("#{attr}=") do |value|
        instance_variable_set(instance_variable_name, sanitize_date(value))
      end

      define_method(attr) do
        instance_variable_get(instance_variable_name)
      end
    end


    def sanitize_date(date)
      date = date.is_a?(Time) ? date : DateTime.parse(date.to_s)
      date.utc
    rescue ArgumentError
      nil
    end

    def attributes
      attributes = {}
      self.class::ATTRS.each do |attr_name|
        attributes[attr_name] = public_send(attr_name)
      end
      attributes
    end
  end
end
