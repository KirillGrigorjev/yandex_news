class AdminNews < News
  ATTRS = (%i(expired_at) | ATTRS).freeze
  DATE_ATTRS = (%i(expired_at) | DATE_ATTRS).freeze

  include ActiveAttributes

  validates :title, presence: true
  validates :annotation, presence: true
  validates :expired_at, presence: true
  validate :expired_at_must_be_in_future

  after_submit :set_current_news
  after_submit :set_expiration_request

  private

  def sanitize_params!(params)
    params.transform_keys!(&:to_sym)
    params[:published_at] = Time.current
    params[:expired_at] = sanitize_date(params[:expired_at])
  end

  def set_current_news
    CurrentNews.refresh_news(attributes, force: true)
  end

  def set_expiration_request
    ExpirationRequestWorker.perform_at(expired_at, uuid)
  end

  def expired_at_must_be_in_future
    return if expired_at.blank?
    errors.add(:expired_at, :must_be_in_future) if Time.current > expired_at
  end

  def set_uuid
    return if changes_empty?
    self.uuid = SecureRandom.uuid
  end
end
