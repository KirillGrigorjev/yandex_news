App.news = App.cable.subscriptions.create("NewsChannel", {
  connected: function() {
  },

  disconnected: function() {
  },

  received: function(data) {
    $('.yn-homepage__waiting').removeClass();
    $('.yn-homepage__news').html(this.renderTemplate(data));
  },

  renderTemplate: function(object) {
    return $('#news-template').render(object);
  }
});
