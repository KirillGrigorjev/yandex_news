class AdminsController < ApplicationController
  before_action :retrieve_news

  def show
  end

  def create
    flash.now[:success] = t('.success') if @news.submit(news_params)
    render :show
  end

  private

  def news_params
    params.require(:admin_news).permit(*AdminNews::ATTRS)
  end

  def retrieve_news
    @news = AdminNews.retrieve_object
  end
end
