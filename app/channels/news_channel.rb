class NewsChannel < ApplicationCable::Channel
  CHANNEL_NAME = 'current_news_channel'.freeze

  def subscribed
    stream_for uuid
    stream_from CHANNEL_NAME
    self.class.broadcast_to(uuid, CurrentNews.retrieve_object.serialize)
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
