# frozen_string_literal: true

module LayoutHelper
  FLASH_TYPES = { success: 'alert-success', error: 'alert-danger', alert: 'alert-warning', notice: 'alert-info' }.freeze

  def bootstrap_class_for(flash_type)
    FLASH_TYPES[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages
    flash.each do |msg_type, message|
      flash_message(message, bootstrap_class_for(msg_type))
    end
    nil
  end

  def flash_message(message, extra_class)
    concat(content_tag(:div, message, class: "alert alert-dismissible top-head #{extra_class}", role: 'alert') do
      concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
        concat content_tag(:span, '&times;'.html_safe, 'aria-hidden' => true)
        concat content_tag(:span, 'Close', class: 'sr-only')
      end)
      concat message.html_safe
    end)
  end
end
