class RefreshNewsWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { minutely }

  def perform
    MainYandexNews.retrieve_object.submit(YandexNewsRss.new(:index).retrieve_fresh_news)
  end
end
