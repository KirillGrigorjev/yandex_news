class ExpirationRequestWorker
  include Sidekiq::Worker

  def perform(uuid)
    admin_news = AdminNews.retrieve_object
    return if uuid != admin_news.uuid
    admin_news.delete
    MainYandexNews.retrieve_object.set_current_news
  end
end
