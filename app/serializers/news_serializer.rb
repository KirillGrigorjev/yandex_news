class NewsSerializer < ActiveModel::Serializer
  attributes :title, :annotation, :published_at

  def published_at
    return unless object.published_at
    I18n.l(object.published_at.in_time_zone('Moscow'.freeze), format: :with_timezone)
  end
end
