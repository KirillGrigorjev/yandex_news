# frozen_string_literal: true

require 'rss'
require 'open-uri'

class YandexNewsRss
  MAPPING = { title: :title, description: :annotation }.freeze
  URL = 'https://news.yandex.ru/'

  def initialize(scope)
    @url = [URL, scope, '.rss'].join
  end

  def retrieve_fresh_news
    open(@url) do |rss|
      map_info(RSS::Parser.parse(rss).items.first)
    end
  end

  def map_info(rss_item)
    result = {}
    MAPPING.each do |external_name, name|
      result[name] = rss_item.public_send(external_name)
    end
    result[:published_at] = rss_item.pubDate.utc
    result[:uuid] = CGI::parse(URI::parse(rss_item.link).query)['cl4url'].first
    result
  end
end
